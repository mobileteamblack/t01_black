//
//  SecondViewController.swift
//  T01_Black
//
//  Created by Clifford Kyle Jarrell on 9/25/18.
//  Copyright © 2018 OSU CS Department. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var btnRotate: UIButton!
    @IBOutlet weak var imgWheelArrow: UIImageView!
    @IBOutlet weak var stackViewKeyboard: UIStackView!
    @IBOutlet weak var Right_swipe: UIImageView!
    @IBOutlet weak var lblCurrentPuzzle: UILabel!

    // allows the reanimation of the wheel
    public var animWheel = 1
    
    // value returned from
    var result: Slice?
    
    // Random Timer everytime
    var time = Double.random(in: 0...5)
    
    // array to represent slices
    var wheel: [Slice]?
    
    // puzzle solution
    let puzzleSolution: String = "team black for president"
    // current unsolved puzzle
    var currentPuzzle: String?
    
    // scores
    @IBOutlet weak var player1Score: UILabel!
    @IBOutlet weak var player2Score: UILabel!
    @IBOutlet weak var player3Score: UILabel!
    
    // Who'se turn it is; player 1, player 2 or player 3.
    var playersTurn = 1
    @IBOutlet weak var PlayerTurnLabel: UILabel!
    
    // process the user's guess
    @IBAction func btnsKeyboard(_ sender: UIButton) {
        print(sender.titleLabel!.text!)
        
        // user cannot guess the same letter
        sender.isHidden = true
    
        let guess = sender.titleLabel!.text!
    
        // If guess occurs in puzzleSolution
        if puzzleSolution.rangeOfCharacter(from: CharacterSet(charactersIn: guess)) != nil {
            // Adjust currentPuzzle so that guess occurs at the same positions in it as it does in puzzleSolution.
            var chars = Array(puzzleSolution)
            for i in 0..<puzzleSolution.count {
                if chars[i] == Character(guess) {
                    // It should never be "default"
                    currentPuzzle = replace(myString: currentPuzzle ?? "default", i, Character(guess))
                }
            }
            setCurPuzzleLabel()
            increasePlayerScore(playerNum: playersTurn)
            
            // Puzzle solved
            if !currentPuzzle!.contains("_") {
                gameOver()
            }
            else {
                showAlert("You got it right! Spin again!")
            }
            
        }
        else {
            showAlert("You got it wrong!")
            updatePlayerTurnLabel()
        }
        
        // prompt the next spin
        showKeyboard(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initWheel()
        
        // Setup toolbar items
        let uibbiFlexLeft = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let uibbiUserPrompt = UIBarButtonItem(title: "Your turn, spin the wheel!", style: .plain, target: self, action: nil)
        let uibbiFlexRight = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        self.toolbarItems = [uibbiFlexLeft, uibbiUserPrompt, uibbiFlexRight]
        
        // initialize currentPuzzle
        currentPuzzle = puzzleSolution.replacingOccurrences(of: "[A-Za-z]", with: "_", options: .regularExpression)
        setCurPuzzleLabel()
    }

      //  Swipe gesture to start the wheel
    @IBAction func Swipe_Gesture(_ sender: UISwipeGestureRecognizer) {
        self.navigationController?.setToolbarHidden(true, animated: true)

        // FIXME / TODO: return a reference to the Slice result of the wheel spin
        result = wheel![Int.random(in: 0..<wheel!.count)]
        
        UIView.animate(withDuration: time, animations: {
            self.imageView.transform = CGAffineTransform(rotationAngle: ((180.0 * .pi) / 180.0) * CGFloat(self.animWheel))
            self.animWheel+=1
        }, completion: {(finished:Bool) in
            
            self.handleWheelSpinResult()
            print("animation complete")
        })
    }
    
    
    // Result of wheel spin determines what the player does next
    func handleWheelSpinResult() {
        
        switch(result!.type) {
            case Slice.standard:
                
                showAlert("Guess a letter for " + String(result!.amount) + " points!")
                showKeyboard(true)
                
                break
            case Slice.bankrupt:
                
                bankruptPlayer(playerNum: playersTurn)
                showAlert("Bankrupt!")
                updatePlayerTurnLabel()
                break
            case Slice.loseTurn:
                
                showAlert("Lose a turn!")
                updatePlayerTurnLabel()
                
                break
            default:
                break
        }
        
    }
    
    // disply views for user input, or display wheel
    func showKeyboard(_ show: Bool) {
        if show == true {
            imageView.isHidden = true
            Right_swipe.isHidden = true
            imgWheelArrow.isHidden = true
            stackViewKeyboard.isHidden = false
        }
        else {
            imageView.isHidden = false
            Right_swipe.isHidden = false
            imgWheelArrow.isHidden = false
            stackViewKeyboard.isHidden = true
        }
    }
    
    // generates simple alert dialog with a given message
    func showAlert(_ message: String) {
        let okAction = UIAlertAction(title: "OK!", style: .default, handler: nil)
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(okAction)  // Present the alert controller to the user.
        present(alertController, animated: true,     completion: nil)
    }
    
    
    // initialize the wheel with Slice objects
    func initWheel() {
        wheel = []
        
        // give it standard slices
        wheel!.append(Slice(type: Slice.standard, 200))
        wheel!.append(Slice(type: Slice.standard, 250))
        wheel!.append(Slice(type: Slice.standard, 300))
        wheel!.append(Slice(type: Slice.standard, 350))
        wheel!.append(Slice(type: Slice.standard, 400))
        wheel!.append(Slice(type: Slice.standard, 500))
        wheel!.append(Slice(type: Slice.standard, 550))
        wheel!.append(Slice(type: Slice.standard, 600))
        wheel!.append(Slice(type: Slice.standard, 700))
        wheel!.append(Slice(type: Slice.standard, 800))
        wheel!.append(Slice(type: Slice.standard, 900))
        wheel!.append(Slice(type: Slice.standard, 1500))
        wheel!.append(Slice(type: Slice.standard, 5000))
        
        // bankrupt and lose a turn
        wheel!.append(Slice(type: Slice.bankrupt))
        wheel!.append(Slice(type: Slice.bankrupt))
        wheel!.append(Slice(type: Slice.loseTurn))
    }
    // Function taken straight from StackOverflow. Replaces the character of a given string with another character at the given position.
    func replace(myString: String, _ index: Int, _ newChar: Character) -> String {
        var chars = Array(myString)     // gets an array of characters
        chars[index] = newChar
        let modifiedString = String(chars)
        return modifiedString
    }
    // increment the player's score by result amount
    func increasePlayerScore(playerNum: Int) {
        var cur: Int
        if playerNum == 1 {
            cur = Int(player1Score.text!)!
            player1Score.text = String(cur + result!.amount)
        }
        else if playerNum == 2 {
            cur = Int(player2Score.text!)!
            player2Score.text = String(cur + result!.amount)
        }
        else {
            cur = Int(player3Score.text!)!
            player3Score.text = String(cur + result!.amount)
        }
    }
    // set players score to 0
    func bankruptPlayer(playerNum: Int) {
        if playerNum == 1 {
            player1Score.text = "0"
        }
        else if playerNum == 2 {
            player2Score.text = "0"
        }
        else {
            player3Score.text = "0"
        }
    }
    // display the next player's turn and keep track of current player
    func updatePlayerTurnLabel() {
        playersTurn += 1
        if playersTurn > 3 {
            playersTurn = 1
        }
        PlayerTurnLabel.text = String(playersTurn)
    }
    
    // puzzle solved, figure the highest score and show alert with the winner.
    func gameOver() {
        
        // calculate highest score
        var most: Int = (Int(player1Score.text!)! > Int(player2Score.text!)!) ? Int(player1Score.text!)! : Int(player2Score.text!)!
        most = (most > Int(player3Score.text!)!) ? most : Int(player3Score.text!)!
        
        // determine the winner
        var winner: String?
        switch(most) {
            case Int(player1Score.text!)!:
                winner = "Player 1"
                break
            case Int(player2Score.text!)!:
                winner = "Player 2"
                break
            case Int(player3Score.text!)!:
                winner = "Player 3"
                break
            default:
                winner = "Player 1"
                break
        }
        
        showAlert(winner! + " wins!")
    }
    
    // sets the current puzzle label makes the puzzle label more readable
    func setCurPuzzleLabel() {
        var curPuzzleWithSpaces = currentPuzzle!
        for i in (1..<curPuzzleWithSpaces.count).reversed() {
            curPuzzleWithSpaces.insert(" ", at: curPuzzleWithSpaces.index(curPuzzleWithSpaces.startIndex, offsetBy: i))
        }
        
        lblCurrentPuzzle.text = curPuzzleWithSpaces
    }
}
