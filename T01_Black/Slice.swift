//
//  Slice.swift
//  T01_Black
//
//  Created by Clifford Kyle Jarrell on 9/27/18.
//  Copyright © 2018 OSU CS Department. All rights reserved.
//

// a list of slices represents the elements in the wheel

import Foundation

public class Slice {
    
    // types of slices in the wheel
    static let standard: Int = 0
    static let bankrupt: Int = 1
    static let loseTurn: Int = 2
    
    public let type: Int
    public let amount: Int
    
    init(type: Int, _ amount: Int = 0) {
        self.type = type
        self.amount = amount
    }
}
